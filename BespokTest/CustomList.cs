﻿using System.Collections.Generic;

namespace BespokTest
{
    class CustomList<T> : List<T> where T: class
    {
        public string DisplayAsTable()
        {
            var result = "<table style=\"width: 100%; border: 1px solid black;\">\n";
            for(var i = 0; i < this.Count; i++)
            {
                var tr = $"<tr>\n<td style=\"border: 1px solid black;\">{i}</td>\n<td style=\"border: 1px solid black;\">{this[i]}</td>\n</tr>\n";
                result += tr;
            }
            result += "</table>";

            return result;
        }
    }
}

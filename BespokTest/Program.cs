﻿using System;

namespace BespokTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var myList = new CustomList<string>();
            myList.Add("John");
            myList.Add("Doe");
            myList.Add("Hello");
            myList.Add("World");

            var asHtml = myList.DisplayAsTable();
            Console.WriteLine(asHtml);
        }
    }
}

select u.user_id, u.first_name, u.last_name, avg(tr.correct) as avg_correct, max(tr.time_taken) as most_recent_test
from users as u
left join test_result as tr on u.user_id = tr.user_id
group by u.user_id,  u.first_name, u.last_name